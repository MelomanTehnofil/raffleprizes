<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Services\AwardingMoneyPrizesService;


class RufflePrizeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Helpers\Contracts\RufflePrizeInterface',
            'App\Http\Services\RufflePrizeService');
        $this->app->bind('App\Helpers\Contracts\FactoryServiceInterface',
            'App\Http\Services\AwardingFactoryService');
        $this->app->bind('App\Helpers\Contracts\FactoryBankServiceInterface',
            'App\Http\Services\Banks\BanksFactoryService');
        $this->app->bind('App\Helpers\Contracts\FactoryTransportServiceInterface',
            'App\Http\Services\Transport\TransportFactoryService');
        $this->app->bind(AwardingMoneyPrizesService::class,
            'App\Http\Services\AwardingMoneyPrizesService');

    }
}
