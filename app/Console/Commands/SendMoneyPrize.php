<?php

namespace App\Console\Commands;

use App\Http\Services\AwardingMoneyPrizesService;
use Illuminate\Console\Command;

class SendMoneyPrize extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'moneyprize:send {N}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Отправляет денежные призы на счета пользователей';

    /**
     * Сервис по отправке денежных призов
     * @var AwardingMoneyPrizesService
     */
    private $awardingMoneyPrizesService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(AwardingMoneyPrizesService $awardingMoneyPrizesService)
    {
        parent::__construct();
        $this->awardingMoneyPrizesService = $awardingMoneyPrizesService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo $this->awardingMoneyPrizesService->massAwarding($this->argument('N'));
    }
}
