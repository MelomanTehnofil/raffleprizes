<?php


namespace App\Http\Services;


use App\Helpers\Contracts\RufflePrizeInterface;

/**
 * Class RufflePrizeService
 * @package App\Http\Services
 * Сервис генерации призов
 */
class RufflePrizeService implements RufflePrizeInterface
{
    /**
     * @return mixed|string
     * Метод генерации призов
     */
    public function prizeDrawing()
    {
        return 'Вы выйграли автомобиль, поздравляем!!!';
    }
}