<?php


namespace App\Http\Services;

use App\Helpers\Contracts\FactoryServiceInterface;

/**
 * Class AwardingFactoryService
 * @package App\Http\Services
 * Класс фарика способов вручения приза
 * Фабрика реализована в самом примитивном представлении
 */
class AwardingFactoryService implements FactoryServiceInterface
{
    /**
     * Денежный приз
     */
    const MONEY_PRIZE = '1';
    /**
     * Бонусные баллы
     */
    const BONUS_PRIZE = '2';

    /**
     * Предмет
     */
    const SUBJECT_PRIZE = '3';

    /**
     * @param $typePrize
     * @return |null
     */
    public function make($typePrize)
    {
        switch ($typePrize) {
            case self::MONEY_PRIZE:
                return \App::make(AwardingMoneyPrizesService::class);
            case self::BONUS_PRIZE:
                return \App::make(AwardingBonusPrizesService::class);
            case self::SUBJECT_PRIZE:
                return \App::make(AwardingSubjectPrizesService::class);
            default:
                return NULL;
        }
    }
}