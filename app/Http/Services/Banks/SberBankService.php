<?php


namespace App\Http\Services\Banks;


use App\Helpers\Contracts\BanksInterface;

/**
 * Class SberBankService
 * @package App\Http\Services\Banks
 * Сервис реализующий взаимодейтвие с АПИ СберБанка
 */
class SberBankService implements BanksInterface
{

    /**
     * @return mixed|string
     * Методы переводит денежный приз на счет
     */
    public function payment()
    {
        return 'Операция выполнена успешено Сбер Банк';
    }
}