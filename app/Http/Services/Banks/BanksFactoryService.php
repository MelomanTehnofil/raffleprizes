<?php


namespace App\Http\Services\Banks;


use App\Helpers\Contracts\FactoryBankServiceInterface;
use App\Helpers\Contracts\FactoryServiceInterface;

/**
 * Class BanksFactoryService
 * @package App\Http\Services\Banks
 * Класс фарика Банков
 * Фабрика реализована в самом примитивном представлении
 */
class BanksFactoryService implements FactoryBankServiceInterface
{
    /*
     * Сбер Банк
     */
    const SBER_BANK = '1';
    /**
     * Альфа банк
     */
    const ALFA_BANK = '2';

    /**
     * @param $type
     * @return mixed
     */
    public function make($type)
    {
        switch ($type) {
            case self::SBER_BANK:
                return \App::make(SberBankService::class);
            case self::ALFA_BANK:
                return \App::make(AlfaBankService::class);
            default:
                return NULL;
        }
    }
}