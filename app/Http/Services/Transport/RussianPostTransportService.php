<?php


namespace App\Http\Services\Transport;


use App\Helpers\Contracts\TransportInterface;

/**
 * Class RussianPostTransportService
 * @package App\Http\Services\Transport
 * Сервис для работы с Почтой России
 */
class RussianPostTransportService implements TransportInterface
{

    /**
     * @return mixed
     * Оформление отправки посылки
     */
    public function sendingCargo()
    {
        return 'Приз отправлен Почтой России';
    }
}