<?php


namespace App\Http\Services\Transport;


use App\Helpers\Contracts\TransportInterface;

/**
 * Class DellinTransportService
 * @package App\Http\Services\Transport
 * Сервис для работы с ТК Деловые линии
 */
class DellinTransportService implements TransportInterface
{

    /**
     * @return mixed
     * Оформление отправки груза
     */
    public function sendingCargo()
    {
        return 'Приз отправлен Деловыми линиями';
    }
}