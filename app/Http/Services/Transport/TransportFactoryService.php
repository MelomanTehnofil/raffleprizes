<?php


namespace App\Http\Services\Transport;


use App\Helpers\Contracts\FactoryTransportServiceInterface;

/**
 * Class TransportFactoryService
 * @package App\Http\Services\Transport
 * Сервис для работы с транспортными компаниями
 */
class TransportFactoryService implements FactoryTransportServiceInterface
{
    const RUSSIAN_POST = '1';
    const DELLIN = '2';
    const PEC = '3';
    const CDEK = '4';

    /**
     * @param $type
     * @return mixed
     */
    public function make($type)
    {
       switch ($type) {
            case self::RUSSIAN_POST:
                return \App::make(RussianPostTransportService::class);
            case self::DELLIN:
                return \App::make(DellinTransportService::class);
            case self::PEC:
                return \App::make(PecTransportService::class);
           case self::CDEK:
                return \App::make(CdekTransportService::class);
            default:
                return NULL;
        }
    }
}