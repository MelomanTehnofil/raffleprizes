<?php


namespace App\Http\Services\Transport;


use App\Helpers\Contracts\TransportInterface;

/**
 * Class CdekTransportService
 * @package App\Http\Services\Transport
 * Сервис для работы с ТК СДЭК
 */
class CdekTransportService implements TransportInterface
{

    /**
     * @return mixed
     * Оформление отправки груза
     */
    public function sendingCargo()
    {
        return 'Приз отправлен через ТК СДЭК';
    }
}