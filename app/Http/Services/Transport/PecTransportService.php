<?php


namespace App\Http\Services\Transport;


use App\Helpers\Contracts\TransportInterface;

/**
 * Class PecTransportService
 * @package App\Http\Services\Transport
 * Сервис для работы с ТК ПЕК
 */
class PecTransportService implements TransportInterface
{

    /**
     * @return mixed
     * Оформление отправки груза
     */
    public function sendingCargo()
    {
        return 'Приз отправлен через ТК ПЕК';
    }
}