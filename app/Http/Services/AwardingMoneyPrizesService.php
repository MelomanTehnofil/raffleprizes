<?php


namespace App\Http\Services;

use App\Helpers\Contracts\AwardingPrizesInterface;
use App\Helpers\Contracts\FactoryBankServiceInterface;
use App\Helpers\Contracts\FactoryServiceInterface;
use App\Http\Services\Banks\BanksFactoryService;

/**
 * Class AwardingMoneyPrizesService
 * @package App\Http\Services
 * Сервис вручения денежных призов
 */
class AwardingMoneyPrizesService implements AwardingPrizesInterface
{

    /**
     * @var AwardingBonusPrizesService
     * Сервис для рабоы с бонусными призами
     */
    private $awardingBonusPrizes;

    /**
     * @var FactoryBankServiceInterface
     * Сервис для работы с банками
     */
    private $bankService;

    /**
     * AwardingMoneyPrizesService constructor.
     * @param AwardingBonusPrizesService $awardingBonusPrizes
     * @param FactoryServiceInterface $bankService
     */
    public function __construct(
        AwardingBonusPrizesService $awardingBonusPrizes,
        FactoryBankServiceInterface $bankService
    )
    {
       $this->awardingBonusPrizes = $awardingBonusPrizes;
       $this->bankService = $bankService;
    }

    /**
     * @return string
     * Вручение денежного приза
     */
    public function hangOver()
    {
        /**
         * Пример отправки перевода в Сбер Банк
         */
        return $this->bankService->make(BanksFactoryService::SBER_BANK)->payment();
        //return 'Денежный приз - вручение';
    }

    /**
     * @param $count
     * @return string
     * Метод отпарвляет денежные призы всем кто раньше их не получал
     * для этого анализируется поле status в таблице PrizesWon
     */
    public function massAwarding($count)
    {
        return 'Денежные призы в количестве ' . $count . " успешно отправлены";
    }

    /**
     * Метод конвертирует деньги в бонусные баллы
     */
    public function convertToPoints()
    {
        /**
         * Код конвертации
         */
        $this->awardingBonusPrizes->hangOver();
    }
}