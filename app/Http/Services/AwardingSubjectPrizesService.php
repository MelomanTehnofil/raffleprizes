<?php


namespace App\Http\Services;


use App\Helpers\Contracts\AwardingPrizesInterface;
use App\Helpers\Contracts\FactoryTransportServiceInterface;
use App\Http\Services\Transport\TransportFactoryService;

/**
 * Class AwardingSubjectPrizesService
 * @package App\Http\Services
 * Сервис выдачи приза - предмет
 */
class AwardingSubjectPrizesService implements AwardingPrizesInterface
{
    /**
     * @var FactoryTransportServiceInterface
     * Сервис ТК
     */
    private $transportService;

    /**
     * AwardingSubjectPrizesService constructor.
     * @param FactoryTransportServiceInterface $transportService
     */
    public function __construct(
        FactoryTransportServiceInterface $transportService
    )
    {
        $this->transportService = $transportService;
    }

    /**
     * @return mixed
     * Метод отпрвляет приз предмет через через сервис ТК
     */
    public function hangOver()
    {
        /**
         * Пример отправки приза почтой россии
         */
        return $this->transportService->make(TransportFactoryService::RUSSIAN_POST)->sendingCargo();
    }
}