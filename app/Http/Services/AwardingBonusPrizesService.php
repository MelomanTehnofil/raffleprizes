<?php


namespace App\Http\Services;


use App\Helpers\Contracts\AwardingPrizesInterface;

/**
 * Class AwardingBonusPrizesService
 * @package App\Http\Services
 * Сервис выдачи приза - Бонусные баллы
 */
class AwardingBonusPrizesService implements AwardingPrizesInterface
{
    /**
     * @return mixed|string
     */
    public function hangOver()
    {
        return 'Бонусные баллы!!!';
    }
}