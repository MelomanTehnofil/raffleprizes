<?php

namespace App\Http\Controllers;

use App\Helpers\Contracts\FactoryServiceInterface;
use App\Helpers\Contracts\RufflePrizeInterface;
use App\Helpers\Contracts\AwardingPrizesInterface;
use Illuminate\Http\Request;


/**
 * Class RafflePrizesController
 * @package App\Http\Controllers
 * Контроллер для генерирует призы с помощью сервиса RufflePrizeService
 */
class RafflePrizesController extends Controller
{
    /**
     * @var RufflePrizeInterface
     */
    private $rufflePrizeService;

    /**
     * @var AwardingPrizesInterface
     */
    private $awardingPrizesFactory;

    /**
     * RafflePrizesController constructor.
     * @param RufflePrizeInterface $rufflePrizeService
     * @param FactoryServiceInterface $awardingPrizesFactory
     */
    public function __construct(
        RufflePrizeInterface $rufflePrizeService,
        FactoryServiceInterface $awardingPrizesFactory
    )
    {
        $this->rufflePrizeService = $rufflePrizeService;
        $this->awardingPrizesFactory = $awardingPrizesFactory;
    }


    /**
     * @param RufflePrizeInterface $rufflePrizeService
     * @return mixed
     * Метод генерирует приз для пользователя
     */
    public function getPrizes()
    {
        return $this->rufflePrizeService->prizeDrawing();
    }

    /**
     * @param Request $request
     * @return mixed
     * Метод выдает приз победителю, способ выдачи определяется по типу type
     */
    public function hangOver(Request $request)
    {
        return $this->awardingPrizesFactory->make($request->get('type'))->hangOver();
    }

}
