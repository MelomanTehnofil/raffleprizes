<?php


namespace App\Helpers\Contracts;

/**
 * Interface AwardingPrizesInterface
 * @package App\Helpers\Contracts
 */
interface AwardingPrizesInterface
{
    /**
     * @return mixed
     * Метод вручения приза
     */
    public function hangOver();
}