<?php


namespace App\Helpers\Contracts;

/**
 * Interface FactoryServiceInterface
 * @package App\Helpers\Contracts
 * Интерфейс фабрики способов вручения приза
 */
interface FactoryServiceInterface
{
    /**
     * @param $type
     * @return mixed
     * Метод получения срвиса вручения приза
     */
    public function make($type);
}