<?php


namespace App\Helpers\Contracts;

/**
 * Interface FactoryBankServiceInterface
 * @package App\Helpers\Contracts
 * Интерфейс фабрики способов вручения приза
 */
interface FactoryBankServiceInterface extends FactoryServiceInterface
{

}