<?php


namespace App\Helpers\Contracts;

/**
 * Interface FactoryTransportServiceInterface
 * @package App\Helpers\Contracts
 * Интерфейс фабрики способов вручения приза
 */
interface FactoryTransportServiceInterface extends FactoryServiceInterface
{

}