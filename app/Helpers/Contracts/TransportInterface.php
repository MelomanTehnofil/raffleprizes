<?php


namespace App\Helpers\Contracts;


/**
 * Interface TransportInterface
 * @package App\Helpers\Contracts
 * ОБщий интерфейс для работы с транспортными компаниями
 */
interface TransportInterface
{
    /**
     * @return mixed
     * Оформление груза для отправки
     */
    public function sendingCargo();
}