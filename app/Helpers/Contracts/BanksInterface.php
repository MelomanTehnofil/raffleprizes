<?php


namespace App\Helpers\Contracts;


/**
 * Interface BanksInterface
 * @package App\Helpers\Contracts
 * Общий интерфес для работы с банками
 */
interface BanksInterface
{
    /**
     * @return mixed
     * Методы выполняет перевод денежных средств на счет победителя
     */
    public function payment();
}