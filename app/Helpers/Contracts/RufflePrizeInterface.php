<?php


namespace App\Helpers\Contracts;

/**
 * Interface RufflePrizeInterface
 * @package App\Helpers\Contracts
 * Интерфейс розыгрыша призов
 */
interface RufflePrizeInterface
{
    /**
     * @return mixed
     * Метод запуска розыгрыша призов
     */
    public function prizeDrawing();
}