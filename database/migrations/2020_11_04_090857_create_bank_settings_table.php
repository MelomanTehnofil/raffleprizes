<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class BankSettingsTable
 * Таблица настроек АПИ банков и Платежных аггрегаторов
 */
class CreateBankSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banksettings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_id')->comment('ID Банка');
            $table->integer('user_id')->nullable()->comment('ID Пользователя ЕСЛИ NULL то для любого');
            $table->string('URI')->nullable()->comment('URI Апи банка');
            $table->text('settings')->nullable()->comment('Специфичные Настройки в JSON формате');
            $table->boolean('is_active')->default(0)->comment('Учавствует в выполнении переводов');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banksettings');
    }
}
