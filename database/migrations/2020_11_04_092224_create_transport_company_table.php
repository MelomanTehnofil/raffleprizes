<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class TransportCompanyTable
 * Таблица транспортных компаний
 */
class CreateTransportCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transportcompanies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('Название Транспортной компании');
            $table->boolean('is_active')->default(0)->comment('Учавствует в отправке призов');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transportcompanies');
    }
}
