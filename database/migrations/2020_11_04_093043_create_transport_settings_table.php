<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class TransportSettingsTable
 * Таблица настроек АПИ Транспортных компаний
 */
class CreateTransportSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transportsettings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tk_id')->comment('ID Транспортной компании');
            $table->integer('user_id')->nullable()->comment('ID Пользователя ЕСЛИ NULL то для любого');
            $table->string('URI')->nullable()->comment('URI Апи Транспортной компании');
            $table->text('settings')->nullable()->comment('Специфичные Настройки в JSON формате');
            $table->boolean('is_active')->default(0)->comment('Учавствует в выполнении отправки призов');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transportsettings');
    }
}
