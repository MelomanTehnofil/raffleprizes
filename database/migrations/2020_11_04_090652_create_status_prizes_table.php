<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateStatusPrizesTable
 * Таблица статусов выйгрыша
 */
class CreateStatusPrizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_prizes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50)->comment('Статус выйгрыша');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_prizes');
    }
}
