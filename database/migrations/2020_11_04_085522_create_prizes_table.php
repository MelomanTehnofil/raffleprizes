<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreatePrizesTable
 * Таблиа призов
 */
class CreatePrizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prizes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('Наименование приза');
            $table->integer('type')->comment('Тип приза');
            $table->integer('counts')->nullable()->comment('Количество');
            $table->boolean('is_active')->default(0)->comment('Учавствует в розыгрыше');
            $table->double('value', 15 ,8)->nullable()->comment('Стоимость');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prizes');
    }
}
