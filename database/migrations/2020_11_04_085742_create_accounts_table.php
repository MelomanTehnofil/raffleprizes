<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateAccountsTable
 * Таблица счетов участвников
 */
class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('account')->comment('Номер счета');
            $table->double('value', 15, 8)->nullable()->comment('Сумма');
            $table->integer('type')->comment('Тип счета');
            $table->boolean('is_active')->default(0)->comment('Учавствует в начислении');
            $table->integer('user_id')->nullable()->comment('ID Пользователя');
            $table->integer('bank_id')->nullable()->comment('ID Банка');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
