<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class PrizesWon
 * Таблица выйгрышей
 */
class CreatePrizesWonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prizeswon', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->comment('ID Пользователя');
            $table->integer('prize_id')->comment('ID Приза');
            $table->integer('status_id')->comment('ID Статуса');
            $table->text('description')->nullable()->comment('Описание/комментарии');
            $table->double('value', 15 ,8)->comment('Стоимость');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prizeswon');
    }
}
