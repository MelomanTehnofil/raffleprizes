-- MySQL dump 10.13  Distrib 5.6.31, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: homestead
-- ------------------------------------------------------
-- Server version	5.6.31-0ubuntu0.15.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Номер счета',
  `value` double(15,8) DEFAULT NULL COMMENT 'Сумма',
  `type` int(11) NOT NULL COMMENT 'Тип счета',
  `is_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Учавствует в начислении',
  `user_id` int(11) DEFAULT NULL COMMENT 'ID Пользователя',
  `bank_id` int(11) DEFAULT NULL COMMENT 'ID Банка',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES (1,'123456987654',NULL,2,1,1,1,NULL,NULL),(2,'1221',NULL,1,1,1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banks`
--

DROP TABLE IF EXISTS `banks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Название банка',
  `bik` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'БИК (Банковский идентификатор)',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banks`
--

LOCK TABLES `banks` WRITE;
/*!40000 ALTER TABLE `banks` DISABLE KEYS */;
INSERT INTO `banks` VALUES (1,'Сбер','12345',NULL,NULL),(2,'Альфабанк','44444',NULL,NULL);
/*!40000 ALTER TABLE `banks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banksettings`
--

DROP TABLE IF EXISTS `banksettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banksettings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bank_id` int(11) NOT NULL COMMENT 'ID Банка',
  `user_id` int(11) DEFAULT NULL COMMENT 'ID Пользователя ЕСЛИ NULL то для любого',
  `URI` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'URI Апи банка',
  `settings` text COLLATE utf8mb4_unicode_ci COMMENT 'Специфичные Настройки в JSON формате',
  `is_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Учавствует в выполнении переводов',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banksettings`
--

LOCK TABLES `banksettings` WRITE;
/*!40000 ALTER TABLE `banksettings` DISABLE KEYS */;
INSERT INTO `banksettings` VALUES (1,1,NULL,'https://online.sberbank.ru/','',1,NULL,NULL),(2,2,NULL,'https://alfabank.ru',NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `banksettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_09_12_130116_create_students_table',1),('2020_11_04_085522_create_prizes_table',2),('2020_11_04_085714_create_type_prizes_table',2),('2020_11_04_085742_create_accounts_table',2),('2020_11_04_085807_create_type_accounts_table',2),('2020_11_04_090553_create_prizes_won_table',3),('2020_11_04_090652_create_status_prizes_table',3),('2020_11_04_090735_create_banks_table',3),('2020_11_04_090857_create_bank_settings_table',4),('2020_11_04_092224_create_transport_company_table',4),('2020_11_04_093043_create_transport_settings_table',4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prizes`
--

DROP TABLE IF EXISTS `prizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prizes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Наименование приза',
  `type` int(11) NOT NULL COMMENT 'Тип приза',
  `counts` int(11) DEFAULT NULL COMMENT 'Количество',
  `is_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Учавствует в розыгрыше',
  `value` double(15,8) DEFAULT NULL COMMENT 'Стоимость',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prizes`
--

LOCK TABLES `prizes` WRITE;
/*!40000 ALTER TABLE `prizes` DISABLE KEYS */;
INSERT INTO `prizes` VALUES (1,'500 рублей',1,10,1,NULL,NULL,NULL),(2,'Автомобиль',3,5,1,500000.00000000,NULL,NULL),(3,'5 баллов',2,40,1,5.00000000,NULL,NULL),(4,'100000',1,3,1,100000.00000000,NULL,NULL),(5,'Мопед',3,4,1,45000.00000000,NULL,NULL);
/*!40000 ALTER TABLE `prizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prizeswon`
--

DROP TABLE IF EXISTS `prizeswon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prizeswon` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'ID Пользователя',
  `prize_id` int(11) NOT NULL COMMENT 'ID Приза',
  `status_id` int(11) NOT NULL COMMENT 'ID Статуса',
  `description` text COLLATE utf8mb4_unicode_ci COMMENT 'Описание/комментарии',
  `value` double(15,8) NOT NULL COMMENT 'Стоимость',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prizeswon`
--

LOCK TABLES `prizeswon` WRITE;
/*!40000 ALTER TABLE `prizeswon` DISABLE KEYS */;
/*!40000 ALTER TABLE `prizeswon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_prizes`
--

DROP TABLE IF EXISTS `status_prizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_prizes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Статус выйгрыша',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_prizes`
--

LOCK TABLES `status_prizes` WRITE;
/*!40000 ALTER TABLE `status_prizes` DISABLE KEYS */;
INSERT INTO `status_prizes` VALUES (1,'Вручен','0000-00-00 00:00:00',NULL),(2,'Ожидает вручения',NULL,NULL),(3,'Отправка транспортной компанией',NULL,NULL),(4,'Отказ',NULL,NULL);
/*!40000 ALTER TABLE `status_prizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transportcompanies`
--

DROP TABLE IF EXISTS `transportcompanies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transportcompanies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Название Транспортной компании',
  `is_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Учавствует в отправке призов',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transportcompanies`
--

LOCK TABLES `transportcompanies` WRITE;
/*!40000 ALTER TABLE `transportcompanies` DISABLE KEYS */;
INSERT INTO `transportcompanies` VALUES (1,'Почта России',1,NULL,NULL),(2,'Деловые линии',1,NULL,NULL),(3,'ПЭК',1,NULL,NULL),(4,'СДЭК',0,NULL,NULL);
/*!40000 ALTER TABLE `transportcompanies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transportsettings`
--

DROP TABLE IF EXISTS `transportsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transportsettings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tk_id` int(11) NOT NULL COMMENT 'ID Транспортной компании',
  `user_id` int(11) DEFAULT NULL COMMENT 'ID Пользователя ЕСЛИ NULL то для любого',
  `URI` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'URI Апи Транспортной компании',
  `settings` text COLLATE utf8mb4_unicode_ci COMMENT 'Специфичные Настройки в JSON формате',
  `is_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Учавствует в выполнении отправки призов',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transportsettings`
--

LOCK TABLES `transportsettings` WRITE;
/*!40000 ALTER TABLE `transportsettings` DISABLE KEYS */;
INSERT INTO `transportsettings` VALUES (1,1,NULL,'https://www.pochta.ru/','',1,NULL,NULL),(2,2,NULL,'https://www.dellin.ru/','',1,NULL,NULL),(3,3,NULL,'https://pecom.ru',NULL,1,NULL,NULL),(4,4,NULL,'https://www.cdek.ru/ru',NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `transportsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_accounts`
--

DROP TABLE IF EXISTS `type_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Тип счета',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_accounts`
--

LOCK TABLES `type_accounts` WRITE;
/*!40000 ALTER TABLE `type_accounts` DISABLE KEYS */;
INSERT INTO `type_accounts` VALUES (1,'Счет Лояльности',NULL,NULL),(2,'Банковский счет',NULL,NULL);
/*!40000 ALTER TABLE `type_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_prizes`
--

DROP TABLE IF EXISTS `type_prizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_prizes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Тип приза',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_prizes`
--

LOCK TABLES `type_prizes` WRITE;
/*!40000 ALTER TABLE `type_prizes` DISABLE KEYS */;
INSERT INTO `type_prizes` VALUES (1,'Денежный','0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'Бонусные баллы',NULL,NULL),(3,'Предмет',NULL,NULL);
/*!40000 ALTER TABLE `type_prizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'test_user','MelomanTehnofil@list.ru','$2y$10$VWXp.ZGIh85X43IlSk19ZeSSV6NpB7LHrK/9x.wavoczbamzXlbja','Y9MhQhz4aYNnf9EfKREP7flB5gsapwypm8SMEq7Ie40Iu506EtDxs0aUAz8q','2020-11-04 00:20:57','2020-11-04 00:20:57');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-05  7:47:37
